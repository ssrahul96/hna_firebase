interface User {
    id: Number;
    username: string;
    password: string;
}


interface PasswordCheck {
    username: string;
    password: string;
}