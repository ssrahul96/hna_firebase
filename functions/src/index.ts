//import libraries
import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as express from 'express';
import * as bodyParser from "body-parser";
var cors = require('cors')
import { gethash, verifyPassword } from './utils/BcryptUtil'
var InvalidUserError = require('./error/InvalidUserError');


//initialize firebase inorder to access its services
admin.initializeApp(functions.config().firebase);

//initialize express server
const app = express();
const main = express();
app.use(cors());
main.use(cors());

//add the path to receive request and set json as bodyParser to process the body 
main.use('/api/v1', app);
main.use(bodyParser.json());
main.use(bodyParser.urlencoded({ extended: true }));

//initialize the database and the collection 
const db = admin.firestore();
const userCollection = 'users';

//define google cloud function name
export const webApi = functions.https.onRequest(main);


app.post('/users', async (req, res) => {
    try {

        let hashPassword = await gethash(req.body["password"]);
        console.log("hashPassword : " + hashPassword)
        let user: User = {
            id: req.body["id"],
            password: hashPassword,
            username: req.body["username"]
        }

        console.log(user);
        await db.collection(userCollection).add(user);
        res.status(201).send(<ResponseModel>{ message: "created" });
    } catch (error) {
        res.status(400).send(<ResponseModel>{ message: error });
    }
});

app.get('/users', async (req, res) => {
    try {
        const userQuerySnapshot = await db.collection(userCollection).get();
        const users: any[] = [];
        userQuerySnapshot.forEach(
            (doc) => {
                users.push(
                    doc.data()
                );
            }
        );
        res.status(200).json(users);
    } catch (error) {
        res.status(500).send(<ResponseModel>{ message: error });
    }
});

app.get('/user/:userId', (req, res) => {

    const userName = req.params.userId;
    db.collectionGroup(userCollection).where("username", "==", userName)
        .get()
        .then(snapshot => {
            if (snapshot.empty) {
                console.log("no user found for username : " + userName);
                res.status(400).json({});
                return;
            }
            snapshot.forEach(user => {
                res.json(user.data());
            })
        })
        .catch(error => res.status(500).send(<ResponseModel>{ message: error }));

});

app.post('/user/verify', async (req, res) => {
    const passwordCheckUser = req.body as PasswordCheck;
    console.log(passwordCheckUser);
    try {
        let userDet: User = await db.collection(userCollection).where("username", "==", passwordCheckUser.username)
            .limit(1)
            .get()
            .then(snapshot => {
                if (snapshot.empty) {
                    res.status(400).send(<ResponseModel>{ message: "no user found for username : " + passwordCheckUser.username });
                }
                var data: any = snapshot.docs.map(function (documentSnapshot) {
                    return documentSnapshot.data();
                });
                return data[0];
            })
        console.log("userDet : " + userDet);
        let isValid = await verifyPassword(passwordCheckUser.password, userDet.password);
        if (isValid) {
            res.send(<ResponseModel>{ message: "valid" });
        } else {
            res.status(403).send(<ResponseModel>{ message: "invalid credentials for user : " + passwordCheckUser.username });
        }
    }
    catch (error) {
        res.status(500).json(<ResponseModel>{ message: error });
    }
});
