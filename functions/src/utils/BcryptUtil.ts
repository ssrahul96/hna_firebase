const bcrypt = require('bcrypt');
const saltRounds = 5;

export async function gethash(plainText: string): Promise<string> {
    return await bcrypt.hashSync(plainText, saltRounds);
}

export async function verifyPassword(plainText: string, hash: string): Promise<boolean> {
    return await bcrypt.compareSync(plainText, hash);
}